#include <iostream>
#include "DataStructures/DynamicQueue.hpp"

int main() {
    DynamicQueue<int> d_que;
    int num, i;
    num = 2222;
    i = 0;
    for(i = 1; i <= 111; i++)
        d_que.enqueue(num++);

    for(i = 1; i <= 110; i++)
        std::cout << "Dequeued element : " << d_que.dequeue() << std::endl;

    return 0;
}
