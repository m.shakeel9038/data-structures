#ifndef DYNAMIC_QUEUE_H
#define DYNAMIC_QUEUE_H
#include <iostream>
#include <cstdlib>
#define MIN_CAPACITY 40

template <typename T>
class DynamicQueue {
    private:
        int front;
        int back;
        int varying_capacity;
        int queue_size;
        T* queues_array;

    public:
        DynamicQueue();
       ~DynamicQueue();
        bool isEmpty();
        bool isFull();
        void enqueue(T);
        T dequeue();
        T peek();
        int getSize();
};

template <typename T>
DynamicQueue<T>::DynamicQueue() {
    front = -1;
    back = 0;
    varying_capacity = MIN_CAPACITY;
    queue_size = 0;
    queues_array = (T*) std::malloc(varying_capacity * sizeof(T));
}

template <typename T>
DynamicQueue<T>::~DynamicQueue() {
    free(queues_array);
}

template <typename T>
bool DynamicQueue<T>::isEmpty() {
    if(queue_size == 0)
        return true;
    return false;
}

template <typename T>
bool DynamicQueue<T>::isFull() {
    if(queue_size == varying_capacity || front == varying_capacity - 1)
        return true;
    return false;
}

template <typename T>
void DynamicQueue<T>::enqueue(T element) {
    if(isFull() == true) {
        varying_capacity += MIN_CAPACITY;
        size_t new_queue_size = varying_capacity * sizeof(T);
        queues_array = (T*) std::realloc(queues_array, new_queue_size);
        if(queues_array == NULL) {
            std::cout << "Error: memory allocation failed!" << std::endl;
            return;
        }
    }
    front += 1;
    queues_array[front] = element;
    queue_size += 1;
}

template <typename T>
T DynamicQueue<T>::dequeue() {
    if(isEmpty() == true)
        throw std::runtime_error("Error: queue is empty!");
    T ele_stored_temp = queues_array[back];
    back += 1;
    queue_size -= 1;
    if(back >= MIN_CAPACITY) {
        int f, b, i, k;
        f = front;
        b = back;
        i = 0;
        k = 0;
        size_t temp_size = (varying_capacity - MIN_CAPACITY) * sizeof(T);
        T* temp_array = (T*) malloc(temp_size);
        if(temp_array == NULL)
            throw std::runtime_error("Error: memory allocation failed!");
        while(b <= f)
            temp_array[i++] = queues_array[b++];

        queues_array = (T*)std::realloc(queues_array, temp_size);
        if(queues_array == NULL)
            throw std::runtime_error("Error: memory allocation failed!");
        while(i > 0) {
            *(queues_array + k) = *(temp_array + k);
            k += 1;
            i -= 1;
        }
        front = front - MIN_CAPACITY;
        back = 0;
        std::free(temp_array);
    }
    return ele_stored_temp;
}

template <typename T>
T DynamicQueue<T>::peek() {
    if(isEmpty() == true)
        throw std::runtime_error("Error: queue is empty!");
    return queues_array[back];
}

template <typename T>
int DynamicQueue<T>::getSize(){
    return queue_size;
}

#endif
