#ifndef STACK_HPP
#define STACK_HPP
#include <iostream>
#include <cstdlib>
#include <cstddef>
#define MIN_CAPACITY 5

template <typename T>
class DynamicStack {
    private:
        int top;
        int varying_capacity;
        int stack_size;
        T* stacks_array;

    public:
        DynamicStack();
       ~DynamicStack();
        bool isEmpty();
        bool isFull();
        void push(T);
        T pop();
        T peek();
        int getSize();
};

template <typename T>
DynamicStack<T>::DynamicStack() {
    top = -1;
    varying_capacity = MIN_CAPACITY;
    stack_size = 0;
    stacks_array = (T*)std::malloc(varying_capacity * sizeof(T));
}

template <typename T>
DynamicStack<T>::~DynamicStack() {
    std::free(stacks_array);
}

template <typename T>
bool DynamicStack<T>::isEmpty() {
    if(top == -1)
        return true;
    return false;
}

template <typename T>
bool DynamicStack<T>::isFull() {
    if(top == MIN_CAPACITY - 1)
        return true;
    return false;
}

/* This function throws runtime exception. */
/* Handle the exception */
template <typename T>
void DynamicStack<T>::push(T element) {
    if(isFull() == true) {
        varying_capacity += MIN_CAPACITY;
        size_t new_mem_size = varying_capacity * sizeof(T);
        stacks_array = (T*) std::realloc(stacks_array, new_mem_size);
        if(stacks_array == NULL) {
            std::cout << "Error: memory allocation failed!" << std::endl;
            return;
        }
    }
    top += 1;
    stack_size += 1;
    stacks_array[top] = element;
}

/* This function throws runtime exception. */
/* Handle the exception */
template <typename T>
T DynamicStack<T>::pop() {
    if(isEmpty() == true) {
        std::runtime_error("Error: Stack underflow!");
    }
    T temp = stacks_array[top];
    if(top == varying_capacity - MIN_CAPACITY - 1 && top > MIN_CAPACITY - 1) {
        varying_capacity -= MIN_CAPACITY;
        size_t new_mem_size = varying_capacity * sizeof(T);
        stacks_array = (T*) std::realloc(stacks_array, new_mem_size);
    }
    top -= 1;
    stack_size -= 1;
    return temp;
}

/* This function throws runtime exception. */
/* Handle the exception */
template <typename T>
T DynamicStack<T>::peek() {
    if(isEmpty() == true) {
        std::runtime_error("Error: Stack empty!");
    }
    return stacks_array[top];
}

template <typename T>
int DynamicStack<T>::getSize() {
    return stack_size;
}

#endif
