#ifndef STACK_HPP
#define STACK_HPP
#include <iostream>
#define MAX_CAPACITY 100

template <typename T>
class Stack {
    private:
        int top;
        int stack_size;
        T stacks_array[MAX_CAPACITY];

    public:
        Stack();
        bool isEmpty();
        bool isFull();
        void push(T);
        T pop();
        T peek();
        int getSize();
};

template <typename T>
Stack<T>::Stack() {
    top = -1;
    stack_size = 0;
}

template <typename T>
bool Stack<T>::isEmpty() {
    if(top == -1)
        return true;
    return false;
}

template <typename T>
bool Stack<T>::isFull() {
    if(top == MAX_CAPACITY - 1)
        return true;
    return false;
}

/* This function throws runtime exception. */
/* Handle the exception */
template <typename T>
void Stack<T>::push(T element) {
    if(isFull() == true) {
        std::runtime_error("Error: Stack overflow!");
        return;
    }
    top += 1;
    stack_size += 1;
    stacks_array[top] = element;
}

/* This function throws runtime exception. */
/* Handle the exception */
template <typename T>
T Stack<T>::pop() {
    if(isEmpty() == true) {
        std::runtime_error("Error: Stack underflow!");
    }
    T temp = stacks_array[top];
    stack_size -= 1;
    top -= 1;
    return temp;
}

/* This function throws runtime exception. */
/* Handle the exception */
template <typename T>
T Stack<T>::peek() {
    if(isEmpty() == true) {
        std::runtime_error("Error: Stack empty!");
    }
    return stacks_array[top];
}

template <typename T>
int Stack<T>::getSize() {
    return stack_size;
}

#endif
