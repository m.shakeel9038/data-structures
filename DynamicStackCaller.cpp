#include <iostream>
#include "DataStructures/DynamicStack.hpp"

int main() {
    DynamicStack<int> s_stk;
    s_stk.push(5);
    s_stk.push(6);
    s_stk.push(7);
    s_stk.push(8);
    s_stk.push(9);
    s_stk.push(10);
    std::cout << "Popped element : " << s_stk.pop() << std::endl;
    std::cout << "Stack size : " << s_stk.getSize() << std::endl;
    std::cout << "Popped element : " << s_stk.pop() << std::endl;
    std::cout << "Stack size : " << s_stk.getSize() << std::endl;
    std::cout << "Popped element : " << s_stk.pop() << std::endl;
    std::cout << "Popped element : " << s_stk.pop() << std::endl;
    std::cout << "Popped element : " << s_stk.pop() << std::endl;
    std::cout << "Stack size : " << s_stk.getSize() << std::endl;
    std::cout << "Popped element : " << s_stk.pop() << std::endl;
    return 0;
}
