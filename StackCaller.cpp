#include <iostream>
#include <string>
#include "DataStructures/Stack.hpp"

int main() {
    Stack<std::string> stk;
    stk.push("India");
    stk.push("Germany");
    stk.push("UK");
    stk.push("Thailand");
    std::cout << "Peeked element : " << stk.peek()    << std::endl;
    std::cout << "Popped element : " << stk.pop()     << std::endl;
    std::cout << "Popped element : " << stk.pop()     << std::endl;
    std::cout << "Stack size : "     << stk.getSize() << std::endl;
    std::cout << "Popped element : " << stk.pop()     << std::endl;
    std::cout << "Popped element : " << stk.pop()     << std::endl;

    return 0;
}
